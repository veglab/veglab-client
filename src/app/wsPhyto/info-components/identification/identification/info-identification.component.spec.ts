import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { InfoIdentificationComponent } from './info-identification.component';

describe('InfoIdentificationComponent', () => {
  let component: InfoIdentificationComponent;
  let fixture: ComponentFixture<InfoIdentificationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoIdentificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoIdentificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
