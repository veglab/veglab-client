import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TableSelectedElementComponent } from './table-selected-element.component';

describe('TableSelectedElementComponent', () => {
  let component: TableSelectedElementComponent;
  let fixture: ComponentFixture<TableSelectedElementComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TableSelectedElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableSelectedElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
