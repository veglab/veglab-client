import { ExtendedFieldModel } from 'src/app/_models/extended-field.model';
import { FieldDataType } from '../../../_enums/field-data-type-enum';

/**
 * Those metadata should always be persisted in DB
 *
 * Default object :
 * {
 *   id: null,
 *   fieldId:           '',
 *   projectName:       'veglab:*',
 *   dataType:          FieldDataType.BOOL,
 *   isVisible:         true,
 *   isEditable:        true,             // nullable
 *   isMandatory:       true,
 *   minValue:          0,                // nullable
 *   maxValue:          1,                // nullable
 *   defaultValue:      '',               // nullable
 *   unit:              '',               // nullable
 *   filterStep:        0.5,              // nullable
 *   filterLogarithmic: false,            // nullable
 *   regexp:            '',               // nullable
 *   extendedFieldTranslations: [{        // nullable
 *     id: null,
 *     projectName: '',
 *     label: '',
 *     description: '',    // nullable
 *     defaultValue: '',   // nullable
 *     errorMessage: '',   // nullable
 *     languageIsoCode: '' // nullable
 *   }]
 * }
 */

const commonMetadata: Array<ExtendedFieldModel> = [
  {
    id: null,
    fieldId: 'from_lobelia',
    projectName: 'veglab:*',
    dataType: FieldDataType.BOOL,
    unit: null,
    minValue: null,
    maxValue: null,
    filterLogarithmic: false,
    isVisible: true,
    isEditable: true,
    isMandatory: false,
    extendedFieldTranslations: [{
      id: null,
      projectName: 'veglab:*',
      label: 'Relevé Lobelia',
      description: 'Relevé issu du SI Lobelia',
      defaultValue: null,
      errorMessage: null,
      languageIsoCode: 'fr'
    }]
  },
  {
    id: null,
    fieldId: 'surface_square_meter',
    projectName: 'veglab:*',
    dataType: FieldDataType.INTEGER,
    unit: 'm²',
    minValue: 0,
    maxValue: 100000000,
    filterLogarithmic: true,
    isVisible: true,
    isEditable: true,
    isMandatory: false,
    extendedFieldTranslations: [{
      id: null,
      projectName: 'veglab:*',
      label: 'Surface',
      description: 'Surface, en mètres carrés',
      defaultValue: null,
      errorMessage: 'La surface doit être un nombre entier compris entre 0 et 100000000',
      languageIsoCode: 'fr'
    }]
  }, {
    id: null,
    fieldId: 'cover_percent',
    projectName: 'veglab:*',
    dataType: FieldDataType.INTEGER,
    unit: '%',
    minValue: 0,
    maxValue: 100,
    isVisible: true,
    isEditable: true,
    isMandatory: false,
    extendedFieldTranslations: [{
      id: null,
      projectName: 'veglab:*',
      label: 'Recouvrement',
      description: 'Recouvrement du relevé, en pourcent',
      defaultValue: null,
      errorMessage: 'La valeur du recouvrement doit être un nombre entier compris entre 0 et 100',
      languageIsoCode: 'fr'
    }]
  }, {
    id: null,
    fieldId: 'inclination_degree',
    projectName: 'veglab:*',
    dataType: FieldDataType.INTEGER,
    unit: '°',
    minValue: 0,
    maxValue: 180,
    isVisible: true,
    isEditable: true,
    isMandatory: false,
    extendedFieldTranslations: [{
      id: null,
      projectName: 'veglab:*',
      label: 'Inclinaison (pente)',
      description: 'Pente du relevé (en degrés)',
      defaultValue: null,
      errorMessage: 'La valeur doit être un nombre entier compris entre 0 et 180',
      languageIsoCode: 'fr'
    }]
  }, {
    id: null,
    fieldId: 'accidental_count',
    projectName: 'veglab:*',
    dataType: FieldDataType.INTEGER,
    unit: '',
    isVisible: true,
    isEditable: true,
    isMandatory: false,
    extendedFieldTranslations: [{
      id: null,
      projectName: 'veglab:*',
      label: 'Accidentelles',
      description: 'Nombre d\'occurrences accidentelles',
      defaultValue: null,
      errorMessage: 'La valeur doit être un nombre entier',
      languageIsoCode: 'fr'
    }]
  }, {
    id: null,
    fieldId: 'annotation',
    projectName: 'veglab:*',
    dataType: FieldDataType.TEXT,
    unit: '',
    isVisible: true,
    isEditable: true,
    isMandatory: false,
    extendedFieldTranslations: [{
      id: null,
      projectName: 'veglab:*',
      label: 'Annotation/remarque',
      description: 'Annotation libre',
      defaultValue: null,
      errorMessage: null,
      languageIsoCode: 'fr'
    }]
  }, {
    id: null,
    fieldId: 'height_meter',
    projectName: 'veglab:*',
    dataType: FieldDataType.DECIMAL,
    unit: 'm',
    minValue: 0,
    maxValue: 40,
    isVisible: true,
    isEditable: true,
    isMandatory: false,
    extendedFieldTranslations: [{
      id: null,
      projectName: 'veglab:*',
      label: 'Hauteur',
      description: 'Hauteur de la végétation',
      defaultValue: null,
      errorMessage: 'La valeur de la hauteur doit être un nombre entier ou flottant compris entre 0 et 40',
      languageIsoCode: 'fr'
    }]
  }
];

const lobeliaMetadata: Array<ExtendedFieldModel> = [
  {
    id: null,
    fieldId: 'lobelia_id_releve',
    projectName: 'veglab:*',
    dataType: FieldDataType.INTEGER,
    isVisible: true,
    isEditable: false,
    isMandatory: true,
    extendedFieldTranslations: [{
      id: null,
      projectName: 'veglab:*',
      label: 'ID Lobelia',
      description: 'ID du relevé dans le SI Lobelia',
      defaultValue: null,
      errorMessage: null,
      languageIsoCode: 'fr'
    }]
  },
  {
    id: null,
    fieldId: 'lobelia_uuid_releve',
    projectName: 'veglab:*',
    dataType: FieldDataType.TEXT,
    regexp: '(([a-zA-Z0-9])*(-))*([a-zA-Z0-9])*',
    isVisible: true,
    isEditable: false,
    isMandatory: true,
    extendedFieldTranslations: [{
      id: null,
      projectName: 'veglab:*',
      label: 'UUID Lobelia',
      description: 'UUID du relevé dans le SI Lobelia',
      defaultValue: null,
      errorMessage: 'Le format doit correspondre à un UUID valide dans le SI Lobelia. Exemple: "gt758580-0147-2g5h-b8b4-1f1545b51558"',
      languageIsoCode: 'fr'
    }]
  }, {
    id: null,
    fieldId: 'lobelia_code_pointage',
    projectName: 'veglab:*',
    dataType: FieldDataType.TEXT,
    isVisible: true,
    isEditable: false,
    isMandatory: true,
    extendedFieldTranslations: [{
      id: null,
      projectName: 'veglab:*',
      label: 'Code pointage Lobelia',
      description: 'Code du pointage dans le SI Lobelia',
      defaultValue: null,
      errorMessage: null,
      languageIsoCode: 'fr'
    }]
  }
];

const pvf2Metadata: Array<ExtendedFieldModel> = [
  {
    id: null,
    fieldId: 'order_pvf2',
    projectName: 'veglab:*',
    dataType: FieldDataType.TEXT,
    regexp: 'O[1-9]',
    isVisible: true,
    isEditable: false,
    isMandatory: true,
    extendedFieldTranslations: [{
      id: null,
      projectName: 'veglab:*',
      label: 'Ordre PVF2',
      description: 'Identifiant de l\'Ordre dans le référentiel PVF2',
      defaultValue: null,
      errorMessage: 'Le format doit correspondre à un identifiant d\'ordre valide. Exemple: "O1"',
      languageIsoCode: 'fr'
    }]
  },
  {
    id: null,
    fieldId: 'alliance_pvf2',
    projectName: 'veglab:*',
    dataType: FieldDataType.TEXT,
    regexp: 'A [0-9]*(.[0-9]*)?',
    isVisible: true,
    isEditable: false,
    isMandatory: true,
    extendedFieldTranslations: [{
      id: null,
      projectName: 'veglab:*',
      label: 'Alliance PVF2',
      description: 'Identifiant de l\'Alliance dans le référentiel PVF2',
      defaultValue: null,
      errorMessage: 'Le format doit correspondre à un identifiant d\'alliance valide. Exemple: "A 1.8"',
      languageIsoCode: 'fr'
    }]
  },
  {
    id: null,
    fieldId: 'fiche_pvf2',
    projectName: 'veglab:*',
    dataType: FieldDataType.TEXT,
    regexp: 'F [0-9]*(-[0-9]*)?',
    isVisible: true,
    isEditable: false,
    isMandatory: true,
    extendedFieldTranslations: [{
      id: null,
      projectName: 'veglab:*',
      label: 'Fiche PVF2',
      description: 'Identifiant de la fiche dans le référentiel PVF2',
      defaultValue: null,
      errorMessage: 'Le format doit correspondre à un identifiant de fiche valide. Exemple: "F 03-01"',
      languageIsoCode: 'fr'
    }]
  },
  {
    id: null,
    fieldId: 'syntaxon_id_pvf2',
    projectName: 'veglab:*',
    dataType: FieldDataType.INTEGER,
    isVisible: true,
    isEditable: false,
    isMandatory: true,
    extendedFieldTranslations: [{
      id: null,
      projectName: 'veglab:*',
      label: 'Identifiant PVF2',
      description: 'Identifiant du syntaxon dans le référentiel PVF2',
      defaultValue: null,
      errorMessage: 'Le format doit correspondre à un identifiant de syntaxon valide. Exemple: "1"',
      languageIsoCode: 'fr'
    }]
  }
];

export const MetadataInitialSet: Array<ExtendedFieldModel> = [
  ...commonMetadata,
  ...pvf2Metadata,
  ...lobeliaMetadata
];
