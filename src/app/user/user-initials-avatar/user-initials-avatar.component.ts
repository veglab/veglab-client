import { Component, Input } from '@angular/core';
import { VlUser } from '../../_models/vl-user.model';

@Component({
  selector: 'vl-user-initials-avatar',
  templateUrl: './user-initials-avatar.component.html',
  styleUrls: ['./user-initials-avatar.component.scss']
})
export class UserInitialsAvatarComponent {
  @Input() user: VlUser;
  @Input() diameterEm = 2.5;
  @Input() options: { showFirstAndLastNames?: boolean, highlight?: boolean, cursor?: string } =
                    { showFirstAndLastNames: false, highlight: false, cursor: 'default' };
}
