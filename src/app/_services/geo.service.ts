import { Injectable } from '@angular/core';
import { CommonGeocodedData } from '../_models/geo/commonGeocodedData.model';
import { VlAccuracyEnum } from '../_models/vlAccuracy.enum';

import * as turf from '@turf/turf';
import { Feature, Point } from '@turf/turf';
import * as _ from 'lodash-es';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { VlGeocodedDataModel, VlReverseGeocodedDataModel } from '../_models/vlGeocodedData.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GeoService {
  constructor(private http: HttpClient) { }

  /**
   * Takes a GeoJSON object and returns a simplified version using turf.js. Internally uses simplify-js to perform simplification using the Ramer-Douglas-Peucker algorithm.
   * @param geoJson the GeoJson object
   * @param tolerance simplification tolerance
   * @param highQuality whether or not to spend more time to create a higher-quality simplification with a different algorithm
   * @param mutate see turf documentation
   */
  simplifyPolygon(geoJson: any, tolerance = 0.0015, highQuality = false, mutate = true): any {
    const geoType: string = geoJson.type;
    if (geoType == null) {
      return geoJson;
    } else if (geoType.toLowerCase() === 'polygon' || geoType.toLowerCase() === 'multipolygon') {
      try {
        return turf.simplify(geoJson, {tolerance, highQuality, mutate});
      } catch (error) {
        return geoJson;
      }
    } else {
      return geoJson;
    }
  }

  /**
   * Return a valid geoJson Polygon from bounding box coordinates
   */
  bboxToPolygon(bbox: Array<number>): {type: string, coordinates: Array<any>} {
   return {
     type: 'Polygon',
     coordinates: [[
       [ Number(bbox[2]), Number(bbox[0]) ],
       [ Number(bbox[3]), Number(bbox[0]) ],
       [ Number(bbox[3]), Number(bbox[1]) ],
       [ Number(bbox[2]), Number(bbox[1]) ],
       [ Number(bbox[2]), Number(bbox[0]) ]
     ]]
   };
  }

  getCentroid(geoJson: {type: string, coordinates: Array<Array<number>>}): Feature<Point> {
    return turf.centroid(geoJson);
  }

  getAccuracyFromCommonGeocodedData(data: CommonGeocodedData): VlAccuracyEnum {
    if (data.city && data.cityCode) {
      return VlAccuracyEnum.CITY;
    } else if (data.countyCode && data.countyCode) {
      return VlAccuracyEnum.COUNTY;
    } else if (data.region && data.regionCode) {
      return VlAccuracyEnum.REGION;
    } else if (data.country && data.countryCode) {
      return VlAccuracyEnum.COUNTRY;
    } else {
      return VlAccuracyEnum.OTHER;
    }
  }

  public geocode(provider: 'vl-geocoder' | 'ign' | 'osm', params: {term: string, country?: string, region?: string, county?: string, limit?: number}): Observable<Array<CommonGeocodedData>> {
    switch (provider) {
      case 'vl-geocoder':
        return this.geocodeSpecificUsingVlGeocoder(params.term, params.country, params.region, params.county, null, params.limit);
      // case 'ign':
      //   return this.geocodeSpecificUsingIGN(params.country, params.county, params.city, params.place, params.limit);
      // case 'osm':
      //   return this.geocodeSpecificUsingOSM(params.country, params.county, params.city, params.place, params.limit);
    }
  }

  public geocodeSpecificUsingVlGeocoder(term: string, countryCode?: string, regionCode?: string, countyCode?: string, level?: 'country'|'region'|'county'|'city', limit?: number | undefined): Observable<Array<CommonGeocodedData>> {
    const apiUrl = `${environment.geoApi.basePath}/geocoder/geocode/${term}`;
    const parameters = [];

    if (countryCode || regionCode || countyCode || level) {
      if (countryCode) { parameters.push(`countryCode=${countryCode}`); }
      if (regionCode) { parameters.push(`regionCode=${regionCode}`); }
      if (countyCode) { parameters.push(`countyCode=${countyCode}`); }
      if (level) { parameters.push(`level=${level}`); }
    }

    let inlineParameters = '';
    if (parameters.length > 0) {
      inlineParameters += '?';
      for (let i = 1; i <= parameters.length; i++) {
        if (i > 1) { inlineParameters += '&'; }
        inlineParameters += parameters[i - 1];
      }
    }

    return this.http.get<Array<VlGeocodedDataModel>>(apiUrl + inlineParameters).pipe(map(response => this.getCommonGeocodedDataFromVlGeocodedData(response)));
  }

  reverseGeocoding(lat: number, lng: number, provider: 'vl-geocoder'|'ign'|'osm'|'mapquest'): Observable<CommonGeocodedData> {
    switch (provider) {
      case 'vl-geocoder':
        return this.reverseGeocodeUsingVlGeocoder(lat, lng);
    }
  }

  reverseGeocodeUsingVlGeocoder(lat: number, lng: number): Observable<CommonGeocodedData> {
    const apiUrl = `${environment.geoApi.basePath}/geocoder/reverse/${lat}/${lng}`;
    return this.http.get<VlReverseGeocodedDataModel>(apiUrl).pipe(map(result => this.getCommonGeocodedDataFromVlReverseGeocodedData(result)));
  }

  /**
   * Get a human-readable address
   * @TODO delete this function and its usages
   */
  getReadableAddress(results: any, provider: 'vl-geocoder'|'ign'|'osm'|'mapquest'): string {
    switch (provider) {
      case 'vl-geocoder':
        return this.getVlGeocoderReadableAddress(results);
    }
  }

  getReadableAddressFromCommonGeocodedData(data: CommonGeocodedData): string {
    const countryPart = data.country && data.countryCode ? `${data.country}` : data.country && !data.countryCode ? `${data.country}` : null;
    const regionPart = data.region && data.regionCode ? `${data.region} (${data.regionCode})` : data.region && !data.regionCode ? `${data.region}` : null;
    const countyPart = data.county && data.countyCode ? `${data.county} (${data.countyCode})` : data.county && !data.countyCode ? `${data.county}` : null;
    const cityPart = data.city && data.cityCode ? `${data.city} (${data.cityCode})` : data.city && !data.cityCode ? `${data.city}` : null;

    const parts: Array<string> = [];
    if (cityPart) { parts.push(cityPart); }
    if (countyPart) { parts.push(countyPart); }
    if (regionPart) { parts.push(regionPart); }
    if (countryPart) { parts.push(countryPart); }
    return parts.join(' | ');
  }

  /**
   * @TODO delete this function and its usages
   */
  getVlGeocoderReadableAddress(vlg: VlGeocodedDataModel): string {
    const countryPart = vlg.country && vlg.countrycode ? `${vlg.country}` : null;
    const regionPart = vlg.region && vlg.regioncode ? `${vlg.region} (${vlg.regioncode})` : null;
    const countyPart = vlg.county && vlg.countycode ? `${vlg.county} (${vlg.countycode})` : null;
    const cityPart = vlg.city && vlg.citycode ? `${vlg.city} (${vlg.citycode})` : null;

    const parts: Array<string> = [];
    if (cityPart) { parts.push(cityPart); }
    if (countyPart) { parts.push(countyPart); }
    if (regionPart) { parts.push(regionPart); }
    if (countryPart) { parts.push(countryPart); }

    return parts.join(' | ');
  }

  getCommonGeocodedDataFromVlGeocodedData(VlGeocodedData: Array<VlGeocodedDataModel>): Array<CommonGeocodedData> {
    const response: Array<CommonGeocodedData> = [];
    for (const geocodedData of VlGeocodedData) {
      response.push({
        id: geocodedData.id,
        level: geocodedData.level,
        countryCode: geocodedData.countrycode,
        country: geocodedData.country && geocodedData.country !== '' ? geocodedData.country : null,
        regionCode: geocodedData.regioncode,
        region: geocodedData.region && geocodedData.region !== '' ? geocodedData.region : null,
        countyCode: geocodedData.countycode,
        county: geocodedData.county && geocodedData.county !== '' ? geocodedData.county : null,
        cityCode: geocodedData.citycode,
        city: geocodedData.city && geocodedData.city !== '' ? geocodedData.city : null,
        postCode: geocodedData.postcode,
        geometry: geocodedData.geojson,
        centroid: geocodedData.centroid
      });
    }
    return response;
  }


  getCommonGeocodedDataFromVlReverseGeocodedData(vlReverseGeocodedData: VlReverseGeocodedDataModel): CommonGeocodedData {
    return {
      id: vlReverseGeocodedData.id,
      level: vlReverseGeocodedData.level,
      cityCode: vlReverseGeocodedData.citycode,
      city: vlReverseGeocodedData.city,
      countyCode: vlReverseGeocodedData.countycode,
      county: vlReverseGeocodedData.county,
      regionCode: vlReverseGeocodedData.regioncode,
      region: vlReverseGeocodedData.region,
      countryCode: vlReverseGeocodedData.countrycode,
      country: vlReverseGeocodedData.country,
      postCode: vlReverseGeocodedData.postcode,
      geometry: vlReverseGeocodedData.geojson,
      centroid: vlReverseGeocodedData.centroid
    };
  }

  /**
   * Sometime, we have to reverse latitude and longitude coordinates according to Leaflet specifications
   */
  reverseCoordinatesArray(coordinatesArray: Array<[number, number]>) {
    if (coordinatesArray.length > 0) {
      coordinatesArray.forEach(item => {
        item.reverse();
      });
      return coordinatesArray;
    }
  }

  reverseMultipolygonCoordinates(coordinates: Array<Array<[number, number]>>): Array<Array<[number, number]>> {
    const clonedCoordinates = _.clone(coordinates);
    for (let arr1 of coordinates) {
      arr1 = this.reverseCoordinatesArray(arr1);
    }
    return clonedCoordinates;
  }

  reverseLinestringCoordinates(coordinates: Array<[number, number]>): Array<[number, number]> {
    return this.reverseCoordinatesArray(coordinates);
  }

  reversePointCoordinates(coordinates: [number, number]): [number, number] {
    return coordinates.reverse() as [number, number];
  }

  /**
   * Returns a simple line (2 points) from a polyline
   */
  simplifyPolyline(coordinatesArray: Array<[number, number]>): Array<[number, number]> {
    if (coordinatesArray.length > 1) {
      const firstCoordinate = coordinatesArray[0];
      const lastCoordinate = coordinatesArray[coordinatesArray.length - 1];
      return [firstCoordinate, lastCoordinate];
    } else {
      return coordinatesArray;
    }
  }

  public displayFn(data: CommonGeocodedData): string {
    return `${
      data.city ? data.city :
        data.county ? data.county :
          data.region ? data.region :
            data.country ? data.country : '?'
    } (${(data.countyCode ? data.countyCode : data.regionCode ? data.regionCode : data.countryCode ? data.countryCode : '')})`;
  }
}
