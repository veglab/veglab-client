import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RData } from '../shared/table-r-clustering/table-r-clustering.component';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RService {

  constructor(private http: HttpClient) { }

  public getCluster(data: RData): Observable<RClusteringResponse> {
    return this.http.post<{ value: RClusteringResponse }>(`${environment.rApi.basePath}/cluster`, data).pipe(
      map(response => response.value)
    );
  }
}

export interface RClusteringResponse {
  graphDendro: string;
  graphHeatmap: string;
  groups: null|[{group: number, releve: string}];
}
