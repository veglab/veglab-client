import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private toastr: ToastrService) { }

  // see options: https://github.com/scttcper/ngx-toastr#options
  toastrGlobalOptions: any = {
    closeButton: true,
    disableTimeOut: false,
    timeOut: 4000,
    progressBar: true,
  };

  getToastConfiguration(type: 'info'|'success'|'warning'|'error') {
    return {
      type,
      ...this.toastrGlobalOptions
    };
  }

  info(message: string) {
    this.toastr.info(message, '', this.getToastConfiguration('info'));
  }

  success(message: string) {
    this.toastr.success(message, '', this.getToastConfiguration('success'));
  }

  warn(message: string) {
    this.toastr.warning(message, '', this.getToastConfiguration('warning'));
  }

  error(message: string) {
    this.toastr.error(message, '', this.getToastConfiguration('error'));
  }
}
