import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Table } from '../_models/table.model';
import { environment } from '../../environments/environment';
import { Observable, of, take } from 'rxjs';
import { map } from 'rxjs/operators';
import { IdentificationModel } from '../_models/identification.model';
import { VlUser } from '../_models/vl-user.model';

@Injectable({
  providedIn: 'root'
})
export class TableTreeService {
  constructor(private http: HttpClient) { }

  getTableTree(table: Table): Observable<null|Array<TableTree>> {
    if (!table || !table.id) {
      return of(null);
    }

    return this.http.get<Array<TableTree>>(`${environment.apiBaseUrl}/tables/${table.id}/tree`).pipe(
      map(result => result['hydra:member']),
      take(1)
    );
  }
}

export interface TableTree {
  level: number;
  table: TableTreeNode;
  children?: Array<TableTree>;
}

export interface TableTreeNode {
  id: number;
  identifications: Array<IdentificationModel>;
  owner: VlUser;
  sye: Array<{
    occurrenceCount: number;
  }>;
  isDiagnosis: boolean;
  createdBy: string;
  createdAt: Date;
  updatedBy: string;
  updatesAt: Date;
  title: string;
  description: string;
}
