import { TestBed } from '@angular/core/testing';

import { TableTreeService } from './table-tree.service';

describe('TableTreeService', () => {
  let service: TableTreeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TableTreeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
