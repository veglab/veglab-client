import { Injectable } from '@angular/core';

import { TableService } from './table.service';
import { IdentificationService } from './identification.service';

import { Table } from '../_models/table.model';

import { JsonFlatTable, JsonFlatTableRow } from '../_models/json-flat-table.model';
import { OccurrenceModel } from '../_models/occurrence.model';
import * as _ from 'lodash-es';

@Injectable({
  providedIn: 'root'
})
export class TableIoService {

  constructor(private tableService: TableService, private identificationService: IdentificationService) { }

  toFlatJson(table: Table): JsonFlatTable {
    try {
      const flatTable: JsonFlatTable = {
        rows: []
      };

      // Meta: releves & syes ids
      const flatRowRelSyeIds: JsonFlatTableRow = {
        type: 'meta',
        label: 'id',
        columns: []
      };
      const flatRowAuthors: JsonFlatTableRow = {
        type: 'meta',
        label: 'Observateur(s)',
        columns: []
      };
      const flatRowDate: JsonFlatTableRow = {
        type: 'meta',
        label: 'Date',
        columns: []
      };
      const flatRowLatitude: JsonFlatTableRow = {
        type: 'meta',
        label: 'Latitude',
        columns: []
      };
      const flatRowLongitude: JsonFlatTableRow = {
        type: 'meta',
        label: 'Longitude',
        columns: []
      };
      const flatRowElevation: JsonFlatTableRow = {
        type: 'meta',
        label: 'Altitude',
        columns: []
      };
      const flatRowCountry: JsonFlatTableRow = {
        type: 'meta',
        label: 'Pays',
        columns: []
      };
      const flatRowCounty: JsonFlatTableRow = {
        type: 'meta',
        label: 'Département',
        columns: []
      };
      const flatRowCity: JsonFlatTableRow = {
        type: 'meta',
        label: 'Commune',
        columns: []
      };
      // @Todo add Repository
      // @Todo add Repository nomen
      const flatRowBiblio: JsonFlatTableRow = {
        type: 'meta',
        label: 'Ref. biblio.',
        columns: []
      };
      for (const sye of table.sye) {
        for (const releve of sye.occurrences) {
          flatRowRelSyeIds.columns.push({
            type: 'occurrence',
            label: releve.id.toString()
          });
          flatRowAuthors.columns.push({
            type: 'occurrence',
            label: releve.vlObservers.map(obs => obs.name).join(';')
          });
          flatRowDate.columns.push({
            type: 'occurrence',
            label: releve.dateObserved.toLocaleString().split('T')[0]
          });
          flatRowCountry.columns.push({
            type: 'occurrence',
            label: releve.countryCode
          });
          flatRowCounty.columns.push({
            type: 'occurrence',
            label: releve.countyCode
          });
          flatRowCity.columns.push({
            type: 'occurrence',
            label: releve.cityCode
          });
          flatRowBiblio.columns.push({
            type: 'occurrence',
            label: releve.bibliographySource
          });
        }
        flatRowRelSyeIds.columns.push({
          type: 'syntheticColumn',
          label: sye.id ? sye.id.toString() : ''
        });
      }
      flatTable.rows.push(flatRowRelSyeIds);
      flatTable.rows.push(flatRowAuthors);
      flatTable.rows.push(flatRowDate);
      flatTable.rows.push(flatRowCountry);
      flatTable.rows.push(flatRowCounty);
      flatTable.rows.push(flatRowCity);
      flatTable.rows.push(flatRowBiblio);

      // Extended fields
      let extFieldsNames: Array<string> = [];
      for (const sye of table.sye) {
        for (const releve of sye.occurrences) {
          for (const releveExtField of releve.extendedFieldOccurrences) {
            console.log(releveExtField);
            extFieldsNames.push(releveExtField.extendedField.fieldId);
          }
        }
      }
      extFieldsNames = _.uniq(extFieldsNames).sort();
      for (const extFieldName of extFieldsNames) {
        const flatRowExtField: JsonFlatTableRow = {
          type: 'meta',
          label: extFieldName,
          columns: []
        };
        for (const sye of table.sye) {
          for (const releve of sye.occurrences) {
            const releveExtField = releve.extendedFieldOccurrences.find(relExtField => relExtField.extendedField.fieldId === extFieldName);
            if (releveExtField) {
              flatRowExtField.columns.push({
                type: 'occurrence',
                label: releveExtField.value
              });
            } else {
              flatRowExtField.columns.push({
                type: 'occurrence',
                label: ''
              });
            }
          }
          flatRowExtField.columns.push({
            type: 'syntheticColumn',
            label: ''
          });
        }
        flatTable.rows.push(flatRowExtField);
      }

      // Content
      for (const rowDefItem of table.rowsDefinition) {
        let flatRowContent: JsonFlatTableRow;
        if (rowDefItem.type === 'group' || rowDefItem.type === 'data') {
          flatRowContent = {
            type: rowDefItem.type,
            repository: rowDefItem?.repository,
            idNomen: rowDefItem?.repositoryIdNomen?.toString(),
            idTaxo: rowDefItem?.repositoryIdTaxo?.toString(),
            layer: rowDefItem?.layer,
            label: rowDefItem.displayName,
            columns: []
          };

          flatTable.rows.push(flatRowContent);
        }
      }

      for (const sye of table.sye) {
        if (!sye.syntheticSye) {
          const releves = sye.occurrences;
          for (const releve of releves) {
            const idioOccurrences: Array<OccurrenceModel> = this.tableService.getChildOccurrences(releve);
            flatTable.rows.filter(ftr => ftr.type !== 'meta').map(flatRow => {
              const idioOccurrence = idioOccurrences.find(idio => idio.layer === flatRow.layer && this.identificationService.getFavoriteIdentification(idio).repositoryIdTaxo === flatRow.idTaxo);
              if (idioOccurrence) {
                flatRow.columns.push({ type: 'occurrence', label: idioOccurrence.coef });
              } else {
                flatRow.columns.push({ type: 'occurrence', label: null });
              }
            });
          }
        }
        // add sye synthetic column data
        flatTable.rows.filter(ftr => ftr.type !== 'meta').map(flatRow => {
          const syntheticItem = sye.syntheticColumn.items.find(syeSyntheticItem => syeSyntheticItem.layer === flatRow.layer && syeSyntheticItem.repositoryIdTaxo === flatRow.idTaxo);
          if (syntheticItem) {
            flatRow.columns.push({ type: 'syntheticColumn', label: syntheticItem.coef});
          } else {
            flatRow.columns.push({ type: 'syntheticColumn', label: null});
          }
        });
      }
      return flatTable;
    } catch (e) {
      console.log(e);
    }
  }

  flatJsonTableToCsv(flatTable: JsonFlatTable): string {
    let csvContent = '';

    // Meta
    flatTable.rows.filter(ftr => ftr.type === 'meta').map(flatRow => {
      csvContent += flatRow.repository ? `"${flatRow.repository}",` : ',';
      csvContent += flatRow.idTaxo ? `"${flatRow.idTaxo}",` : ',';
      csvContent += flatRow.layer ? `"${flatRow.layer}",` : ',';
      csvContent += flatRow.label ? `"${flatRow.label}",` : ',';
      for (let i = 0; i < flatRow.columns.length; i++) {
        const separator = i < flatRow.columns.length - 1 ? ',' : '';
        csvContent += flatRow.columns[i].label ? `"${flatRow.columns[i].label}"${separator}` : `${separator}`;
      }
      csvContent += '\n';
    });

    // Content header
    csvContent += 'Référentiel,Nomen,Strate,';
    for (let i = 0; i < flatTable.rows[0].columns.length; i++) { csvContent += ','; }
    csvContent += '\n';

    // Content
    flatTable.rows.filter(ftr => ftr.type !== 'meta').map(flatRow => {
      csvContent += flatRow.repository ? `"${flatRow.repository}",` : ',';
      csvContent += flatRow.idTaxo ? `"${flatRow.idTaxo}",` : ',';
      csvContent += flatRow.layer ? `"${flatRow.layer}",` : ',';
      csvContent += flatRow.label ? `"${flatRow.label}",` : ',';
      for (let i = 0; i < flatRow.columns.length; i++) {
        const separator = i < flatRow.columns.length - 1 ? ',' : '';
        csvContent += flatRow.columns[i].label ? `"${flatRow.columns[i].label}"${separator}` : `${separator}`;
      }
      csvContent += '\n';
    });
    return csvContent;
  }
}
