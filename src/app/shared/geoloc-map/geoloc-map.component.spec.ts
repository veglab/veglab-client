import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GeolocMapComponent } from './geoloc-map.component';

describe('GeolocMapComponent', () => {
  let component: GeolocMapComponent;
  let fixture: ComponentFixture<GeolocMapComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GeolocMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeolocMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
