import { Component, OnDestroy, OnInit } from '@angular/core';

import { TableTree, TableTreeNode, TableTreeService } from '../../_services/table-tree.service';
import { TableService } from '../../_services/table.service';
import { NotificationService } from '../../_services/notification.service';
import { UserService } from '../../_services/user.service';

import { Table } from '../../_models/table.model';
import { VlUser } from '../../_models/vl-user.model';

import { Subscription, take } from 'rxjs';

@Component({
  selector: 'vl-table-tree-hierarchy',
  templateUrl: './table-tree-hierarchy.component.html',
  styleUrls: ['./table-tree-hierarchy.component.scss']
})
export class TableTreeHierarchyComponent implements OnInit, OnDestroy {
  tableSubscription: Subscription;
  currentUser: VlUser;
  currentTable: Table;
  tableTree: Array<TableTree>;
  isLoadingTree: boolean;
  isLoadingTable: boolean;
  isLoadingTableId: number|null;

  constructor(private tableTreeService: TableTreeService,
              private tableService: TableService,
              private notificationService: NotificationService,
              private userService: UserService) {
    this.isLoadingTree = false;
    this.isLoadingTable = false;
    this.isLoadingTableId = null;
  }
  ngOnInit() {
    this.currentUser = this.userService.currentVlUser.getValue();
    this.currentTable = this.tableService.getCurrentTable();
    this.update();

    this.tableSubscription = this.tableService.currentTableChanged.subscribe(tableChanged => {
      this.currentTable = this.tableService.getCurrentTable();
      this.update();
    });
  }

  ngOnDestroy() {
    if (this.tableSubscription) { this.tableSubscription.unsubscribe(); }
  }

  private update() {
    if (this.tableService.isTableEmpty(this.currentTable)) {
      this.tableTree = null;
      this.isLoadingTree = false;
      return;
    }
    this.tableTree = null;
    this.isLoadingTree = true;
    if (this.currentTable && this.currentTable.id) {
      this.tableTreeService.getTableTree(this.tableService.getCurrentTable()).subscribe(tree => {
        this.isLoadingTree = false;
        this.tableTree = tree;
      }, error => {
        this.isLoadingTree = false;
        this.notificationService.error('Nous ne parvenons pas à charger l\'arborescence des tableaux');
      });
    }
  }

  countOccurrences(table: TableTreeNode): number|null {
    if (!table || !table.sye || table.sye.length === 0) {
      return null;
    }

    let count = 0;
    table.sye.map(sye => count += sye.occurrenceCount);
    return count;
  }

  setCurrentTable(tableId: number) {
    this.isLoadingTableId = tableId;
    this.tableService.getTableById(tableId).pipe(
      take(1)
    ).subscribe(newTable => {
      this.tableService.setCurrentTable(newTable, true);
      this.isLoadingTableId = null;
    }, error => {
      this.isLoadingTableId = null;
      this.notificationService.error(`Nous ne parvenons pas à charger le tableau n°${tableId}`);
    });
  }
}
