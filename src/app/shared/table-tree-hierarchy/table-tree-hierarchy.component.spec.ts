import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableTreeHierarchyComponent } from './table-tree-hierarchy.component';

describe('TableTreeHierarchyComponent', () => {
  let component: TableTreeHierarchyComponent;
  let fixture: ComponentFixture<TableTreeHierarchyComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TableTreeHierarchyComponent]
    });
    fixture = TestBed.createComponent(TableTreeHierarchyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
