import { Component, OnInit } from '@angular/core';
import { UntypedFormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { environment } from '../../../environments/environment';
import { SsoService } from 'src/app/_services/sso.service';
import { WorkspaceService } from 'src/app/_services/workspace.service';
import { MenuService } from 'src/app/_services/menu.service';

import { UserService } from '../../_services/user.service';
import { loginMenu } from '../../_menus/main-menus';

@Component({
  selector: 'vl-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  hidePassword = true;

  email = new UntypedFormControl('', [Validators.required, Validators.email]);
  password = new UntypedFormControl('', [Validators.required]);

  autoLogging = true;
  autoLoggingSuccessful = false;
  isLogging = false;
  errorMessage: string = null;
  private readonly unsetToken = environment.app.unsetTokenValue;

  redirectUrl: string = null;

  userSubscription = new Subscription();

  constructor(private ssoService: SsoService,
              private userService: UserService,
              private routerService: Router,
              private route: ActivatedRoute,
              private wsService: WorkspaceService,
              private menuService: MenuService) { }

  ngOnInit() {
    this.redirectUrl = this.route.snapshot.queryParams['redirectUrl'] || '/';
    this.wsService.currentWS.next('none');
    this.menuService.setMenu(loginMenu);

    this.userSubscription = this.userService.currentVlUser.subscribe(nextVlUser => {
      // We subscribe to vl user change: at app startup, user can be redirected to the login page (Auth guard) while refreshing the token
      // The login() function will try to refresh the token and the user service will emit the vl user
      this.autoLogging = true;
      if (nextVlUser) {
        setTimeout(() => {
          this.autoLoggingSuccessful = true;
          setTimeout(() => {
            this.routerService.navigateByUrl(this.redirectUrl);
            this.autoLogging = false;
          }, 800); // A: checkmark duration
        }, 800); // B: spinner duration
      } else {
        setTimeout(() => {
          this.autoLogging = false;
        }, 2000); // C: time to wait for the auto logging process (before to show the form) ; C > A + B
      }
    });
  }

  getErrorMessage(): string {
    return this.email.hasError('required') ? 'Ce champ est obligatoire' :
      this.email.hasError('email') ? 'Ce courriel n\'est pas valide' :
        '';
  }

  isFormComplete(): boolean {
    return this.email.valid && this.password.valid;
  }

  login(): void {
    this.isLogging = true;
    this.errorMessage = null;
    this.ssoService.loginWithEmailAndPassword(this.email.value, this.password.value).subscribe(
      response => {
        if (response && response.access_token) {
          // At this point, we should have a valid identite.token value
          this.ssoService.setToken(response.access_token);
          this.ssoService.setRefreshToken(response.refresh_token);

          // Refresh the token periodically
          this.ssoService.alwaysRefreshToken();

          this.isLogging = false;
          this.routerService.navigateByUrl(this.redirectUrl);
        } else {
          // For any reason (?), we don't get the token...
          this.isLogging = false;
        }
      }, error => {
        if (error.status) {
          switch (error.status) {
            case 401:
              // Not logged, email or password error
              this.errorMessage = 'Courriel ou mot de passe non valide';
              break;
            case 500:
              // Server error
              this.errorMessage = 'Le serveur de connexion ne répond pas';
              // @Todo manage error : log
              break;
            default:
              this.errorMessage = 'Erreur inconnue';
              break;
          }
        }
        this.isLogging = false;
        console.log(error);
      }
    );
  }

  onPasswordKeyup(event: KeyboardEvent): void {
    if (event.key === 'Enter') {
      this.login();
    }
  }

  onHidePasswordKeypress(event: KeyboardEvent): void {
    event.preventDefault();
  }

}
