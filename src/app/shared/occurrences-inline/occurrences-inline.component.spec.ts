import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OccurrencesInlineComponent } from './occurrences-inline.component';

describe('OccurrencesInlineComponent', () => {
  let component: OccurrencesInlineComponent;
  let fixture: ComponentFixture<OccurrencesInlineComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OccurrencesInlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OccurrencesInlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
