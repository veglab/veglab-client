import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TableIdentificationsPreviewComponent } from './table-identifications-preview.component';

describe('TableIdentificationsPreviewComponent', () => {
  let component: TableIdentificationsPreviewComponent;
  let fixture: ComponentFixture<TableIdentificationsPreviewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TableIdentificationsPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableIdentificationsPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
