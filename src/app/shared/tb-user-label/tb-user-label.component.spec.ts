import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TbUserLabelComponent } from './tb-user-label.component';

describe('TbUserLabelComponent', () => {
  let component: TbUserLabelComponent;
  let fixture: ComponentFixture<TbUserLabelComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TbUserLabelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TbUserLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
