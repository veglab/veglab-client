import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { HomePageComponent } from './home-page.component';
import { MenuService } from '../../_services/menu.service';
import { TableService } from '../../_services/table.service';
import { OccurrenceService } from '../../_services/occurrence.service';
import { WorkspaceService } from '../../_services/workspace.service';
import { Router } from '@angular/router';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { SsoService } from '../../_services/sso.service';
import { ToastrModule } from 'ngx-toastr';


describe('HomePageComponent', () => {
  let component: HomePageComponent;
  let fixture: ComponentFixture<HomePageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ToastrModule.forRoot()],
      declarations: [ HomePageComponent ],
      providers: [MenuService, TableService, SsoService, OccurrenceService, WorkspaceService, Router, HttpClient, HttpHandler]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
