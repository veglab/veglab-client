import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { IdentificationInlineComponent } from './identification-inline.component';

describe('IdentificationInlineComponent', () => {
  let component: IdentificationInlineComponent;
  let fixture: ComponentFixture<IdentificationInlineComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ IdentificationInlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IdentificationInlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
