import { Component, OnInit, Input } from '@angular/core';

import * as _ from 'lodash-es';
import { EsOccurrenceModel } from '../../../_models/es-occurrence-model';

@Component({
  selector: 'vl-occurrence-render-preview',
  templateUrl: './occurrence-render-preview.component.html',
  styleUrls: ['./occurrence-render-preview.component.scss']
})
export class OccurrenceRenderPreviewComponent implements OnInit {
  @Input() set occurrence(value: EsOccurrenceModel) {
    this.occurrencePreview = value !== null && value.childrenPreview && value.childrenPreview.length > 0 ? _.clone(value.childrenPreview) : [];
    this.occurrencePreview.sort((a, b) => a['name'] < b['name'] ? -1 : 1 );
  }

  occurrencePreview: Array<any> = [];

  constructor() { }

  ngOnInit() { }

}
