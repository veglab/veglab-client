import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OccurrenceRenderPreviewComponent } from './occurrence-render-preview.component';

describe('OccurrenceRenderPreviewComponent', () => {
  let component: OccurrenceRenderPreviewComponent;
  let fixture: ComponentFixture<OccurrenceRenderPreviewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OccurrenceRenderPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OccurrenceRenderPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
