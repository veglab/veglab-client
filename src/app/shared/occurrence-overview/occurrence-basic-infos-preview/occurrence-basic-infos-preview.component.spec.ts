import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OccurrenceBasicInfosPreviewComponent } from './occurrence-basic-infos-preview.component';

describe('OccurrenceBasicInfosPreviewComponent', () => {
  let component: OccurrenceBasicInfosPreviewComponent;
  let fixture: ComponentFixture<OccurrenceBasicInfosPreviewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ OccurrenceBasicInfosPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OccurrenceBasicInfosPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
