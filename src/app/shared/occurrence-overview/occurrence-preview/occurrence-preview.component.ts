import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GeoJsonObject } from 'geojson';
import { EsOccurrenceModel } from '../../../_models/es-occurrence-model';

@Component({
  selector: 'vl-occurrence-preview',
  templateUrl: './occurrence-preview.component.html',
  styleUrls: ['./occurrence-preview.component.scss']
})
export class OccurrencePreviewComponent implements OnInit {
  @Input() occurrence: EsOccurrenceModel;
  @Input() options = { showCloseButton: true, shortHeader: false };

  @Output() close = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {
  }

  getGeoJsonArray(occurrence: EsOccurrenceModel): Array<GeoJsonObject> {
    if (occurrence !== null && occurrence.geometry) {
      return [occurrence.geometry];
    } else {
      return [];
    }
  }

  closeMe() {
    this.close.next(true);
  }

}
