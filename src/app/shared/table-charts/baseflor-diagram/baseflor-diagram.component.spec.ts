import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BaseflorDiagramComponent } from './baseflor-diagram.component';

describe('BaseflorDiagramComponent', () => {
  let component: BaseflorDiagramComponent;
  let fixture: ComponentFixture<BaseflorDiagramComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseflorDiagramComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseflorDiagramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
