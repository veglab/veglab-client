import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BaseflorDiagramSelectorComponent } from './baseflor-diagram-selector.component';

describe('BaseflorDiagramSelectorComponent', () => {
  let component: BaseflorDiagramSelectorComponent;
  let fixture: ComponentFixture<BaseflorDiagramSelectorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseflorDiagramSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseflorDiagramSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
