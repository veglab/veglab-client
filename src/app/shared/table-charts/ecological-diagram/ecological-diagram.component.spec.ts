import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EcologicalDiagramComponent } from './ecological-diagram.component';

describe('EcologicalDiagramComponent', () => {
  let component: EcologicalDiagramComponent;
  let fixture: ComponentFixture<EcologicalDiagramComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EcologicalDiagramComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EcologicalDiagramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
