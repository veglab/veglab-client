import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';

import { UserService } from 'src/app/_services/user.service';
import { TableService } from 'src/app/_services/table.service';
import { NotificationService } from 'src/app/_services/notification.service';
import { PdfFileService } from 'src/app/_services/pdf-file.service';

import { Table } from 'src/app/_models/table.model';
import { IdentificationModel } from '../../_models/identification.model';
import { PdfFile } from 'src/app/_models/pdf-file.model';
import { TableRelatedSyntaxon } from 'src/app/_models/table-related-syntaxon';
import { UserModel } from 'src/app/_models/user.model';
import { VlUser } from 'src/app/_models/vl-user.model';

import { RepositoryItemModel } from '../../_models/repository-item.model';
import { FileData } from '../../_models/fileData';

import * as _ from 'lodash-es';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'vl-table-form',
  templateUrl: './table-form.component.html',
  styleUrls: ['./table-form.component.scss']
})
export class TableFormComponent implements OnInit, OnDestroy {
  @Input() title = 'Enregistrer le tableau';
  @Input() options: any = { setOwnerAndOccurrencesAsIRIs: true };

  // ---
  // VAR
  // ---
  tableForm: UntypedFormGroup;
  maxTitleLength = 100;
  maxDescriptionLength = 300;
  currentUser: UserModel;
  currentVlUser: VlUser;
  userSubscription: Subscription;
  vlUserSubscription: Subscription;
  relatedPdfFile: Array<FileData> = [];
  allowedUploadedFileTypes = ['pdf'];

  pdfFilesToSend: Array<FormData> = [];
  pdfFileIrisToLink: Array<string> = [];
  currentTablePdfFiles: Array<PdfFile> = [];
  // pdfFileIrisToUnlink: Array<string> = [];

  uploadingPdf = false;
  postingOrPutingTable = false;

  constructor(
    private userService: UserService,
    public tableService: TableService,
    private pdfFileService: PdfFileService,
    private notificationService: NotificationService,
    private router: Router) { }

  ngOnInit() {
    // Get current user
    this.currentUser = this.userService.currentUser.getValue();
    this.currentVlUser = this.userService.currentVlUser.getValue();
    if (this.currentUser == null) {
      // No user
      // Should refresh the token ?
      // this.notificationService.warn('Il semble que vous ne soyez plus connecté. Nous ne pouvons pas poursuivre l\'enregistrement du tableau.');
      // return;
    }

    // Subscribe to current user
    this.userSubscription = this.userService.currentUser.subscribe(
      user => {
        this.currentUser = user;
      },
      error => {
        // @Todo manage error
      }
    );
    this.vlUserSubscription = this.userService.currentVlUser.subscribe(
      vlUser => {
        this.currentVlUser = vlUser;
      }, error => { console.log(error); }
    );

    this.tableForm = new UntypedFormGroup({
      createdAt: new UntypedFormControl({value: new Date(), disabled: true}, [Validators.required]),
      // createdBy: new FormControl(user, [Validators.required]),
      isDiagnosis: new UntypedFormControl(false, [Validators.required]),
      title: new UntypedFormControl(''),
      description: new UntypedFormControl(''),
      biblioSource: new UntypedFormControl('')
    });

    // if current table has id -> bind form
    const ct = this.tableService.getCurrentTable();
    if (!this.tableService.isTableEmpty(ct) && ct.id) {
      this.bindForm(ct);
    }

    // if current table has pdf file
    if (!this.tableService.isTableEmpty(ct) && ct.pdf) {
      this.currentTablePdfFiles.push(ct.pdf);
    }
  }

  ngOnDestroy() {
    if (this.userSubscription) { this.userSubscription.unsubscribe(); }
  }

  deleteRelatedPdfFile(data: FileData): void {
    this.pdfFilesToSend = [];
    this.relatedPdfFile = [];
  }

  deleteTablePdfFile(pdfFile: PdfFile): void {
    _.remove(this.currentTablePdfFiles, pf => _.isEqual(pf, pdfFile));
  }

  /**
   * On PDF file upload
   * @param data see vl-dropfile-box output event
   */
  pdfUploaded(data: FileData): void {
    this.relatedPdfFile = [data[0]];
    try {
      const fd = this.createFormDataForPdfFile([this.relatedPdfFile[0].file]);
      this.pdfFilesToSend[0] = fd;
    } catch (error) {
      console.log(error);
    }
  }

  private createFormDataForPdfFile(files: Array<File>): FormData {
    const fd = new FormData();
    for (const file of files) {
      fd.append('file', file, file.name);
    }
    return fd;
  }

  /**
   * POST pdf files trough API and then POST / PUT table
   */
  postPdfFiles(callback: 'POST' | 'PUT') {
    if (this.pdfFilesToSend.length > 0) {
      this.uploadingPdf = true;
      this.pdfFileService.createPdfFile(this.pdfFilesToSend[0]).subscribe(
        pdfFile => {
          this.uploadingPdf = false;
          this.postingOrPutingTable = true;
          this.pdfFileIrisToLink.push(pdfFile['@id']);
          if (callback === 'POST') { this.postTable(); }
          if (callback === 'PUT') { this.putTable(); }
        }, errorPdfFile => {
          this.notificationService.error('Erreur lors de l\'upload du fichier  PDF ' + this.pdfFilesToSend[0].get('file').toString());
        }
      );
    }
  }

  /**
   * Create (POST) a new table
   */
  postTable() {
    let prePostedTable = _.cloneDeep(this.tableService.getCurrentTable());

    // Check user is binded to table and syes (it should !)
    if (this.currentVlUser == null) {
      this.notificationService.error('Une erreur est survenue : nous ne parvenons pas à vous identifier correctement');
      return;
    }
    if (prePostedTable.owner == null) {
      prePostedTable.owner = this.currentVlUser;
      if (prePostedTable.syntheticColumn && prePostedTable.syntheticColumn.owner == null) { prePostedTable.syntheticColumn.owner = this.currentVlUser; }
      for (const sye of prePostedTable.sye) {
        if (sye.owner == null) { sye.owner = this.currentVlUser; }
        if (sye.syntheticColumn && sye.syntheticColumn.owner == null) { sye.syntheticColumn.owner = this.currentVlUser; }
      }
    }

    // Bind metadata
    this.bindMetadataToTable(prePostedTable);

    // diagnosis
    if (this.tableForm.controls.isDiagnosis.value === true) { prePostedTable.isDiagnosis = true; } else { prePostedTable.isDiagnosis = false; }

    if (this.pdfFileIrisToLink.length > 0) {
      prePostedTable.pdf = this.pdfFileIrisToLink[0] as any;
    }

    if (this.options.setOwnerAndOccurrencesAsIRIs) {
      // Set occurrences & owners as IRIs to decrease JSON size before POST request...
      // Note: Do not apply when importing data (new occurrences needs to be created)
      prePostedTable = this.tableService.setTableOccurrencesAndOwnersAsIris(prePostedTable);
    }

    // POST table
    this.postingOrPutingTable = true;
    this.tableService.postTable(prePostedTable).subscribe(
      postedTable => {
        this.pdfFileIrisToLink = [];
        this.tableService.setCurrentTable(postedTable, true);
        this.tableService.isTableDirty.next(false);
        this.postingOrPutingTable = false;
        this.notificationService.success('Le tableau a été enregistré');

        // 'Close' action panel
        this.router.navigate(['/phyto/app']);

      }, errorPostedTable => {
        this.postingOrPutingTable = false;
        this.notificationService.error('Nous ne parvenons pas à enregistrer le tableau');
        console.log(errorPostedTable);
      }
    );
  }

  /**
   * Replace (PUT) an existing table
   */
  putTable() {
    let tableToSave = _.cloneDeep(this.tableService.getCurrentTable());
    tableToSave = this.tableService.setTableOccurrencesAndOwnersAsIris(tableToSave);

    tableToSave.updatedAt = new Date();

    // Bind metadata
    this.bindMetadataToTable(tableToSave);

    // is diagnosis
    if (this.tableForm.controls.isDiagnosis.value === true) { tableToSave.isDiagnosis = true; } else { tableToSave.isDiagnosis = false; }

    // Bind pdf files
    let deleteLinkedPdfFile = false;
    if (tableToSave.pdf && this.pdfFileIrisToLink.length > 0) {
      // replace existing pdf file
      const existingPdfFileId = tableToSave.pdf.id;
      tableToSave.pdf[0] = this.pdfFileIrisToLink[0];
      tableToSave.pdf[0].id = existingPdfFileId;
    } else if (tableToSave.pdf && this.currentTablePdfFiles.length === 0) {
      // remove linked pdf files
      deleteLinkedPdfFile = true;
    } else if (tableToSave.pdf == null && this.pdfFileIrisToLink.length > 0) {
      // Create a new pdf file linked to the table
      tableToSave.pdf = this.pdfFileIrisToLink[0] as any;
    }

    // PUT table
    this.postingOrPutingTable = true;
    this.tableService.putTable(tableToSave).subscribe(
      patchedTable => {
        // pdf file to unlink ?
        if (deleteLinkedPdfFile) {
          this.pdfFileService.removePdfFile(patchedTable.pdf.id).subscribe(
            removedPdfFile => {
              patchedTable.pdf = null;
              this.tableService.setCurrentTable(patchedTable, true);
              this.tableService.isTableDirty.next(false);
              this.postingOrPutingTable = false;
              this.notificationService.success('Le tableau a été enregistré');

              // 'Close' action panel
              this.router.navigate(['/phyto/app']);

            }, errorRemovedPdfFile => {
              console.log(errorRemovedPdfFile);
              this.postingOrPutingTable = false;
              this.notificationService.error('Nous ne parvenons pas à supprimer le fichier PDF lié au tableau');
              this.tableService.setCurrentTable(patchedTable, true);
            }
          );
        } else {
          this.tableService.setCurrentTable(patchedTable, true);
          this.postingOrPutingTable = false;
          this.notificationService.success('Le tableau a été enregistré');

          // 'Close' action panel
          this.router.navigate(['/phyto/app']);

        }
      }, errorPatchedTable => {
        this.postingOrPutingTable = false;
        console.log(errorPatchedTable);
        this.notificationService.error('Nous ne parvenons pas à enregistrer le tableau');
      }
    );
  }

  saveTable() {
    const ct = this.tableService.getCurrentTable();
    // POST pdf file
    let pdfFilesToPost = false;
    if (this.pdfFilesToSend.length > 0) { pdfFilesToPost = true; }

    const callback = ct.id ? 'PUT' : 'POST';

    if (pdfFilesToPost) {
      this.postPdfFiles(callback);
    } else {
      if (callback === 'POST') {
        this.postTable();
      } else {
        this.putTable();
      }
    }
  }

  private getIdentificationModelFromRepositoryItemModel(rim: RepositoryItemModel): IdentificationModel {
    const name = rim.name + (rim.author && rim.author !== '' ? ' ' + rim.author : '');
    const ovm: IdentificationModel = {
      citationName: name,
      repository: rim.repository,
      repositoryIdNomen: +rim.idNomen,
      repositoryIdTaxo: rim.idTaxo.toString(),
      nomenclaturalName: name,
      taxonomicalName: name,
      createdAt: new Date(),
      owner: this.currentVlUser
    };
    return ovm;
  }

  bindMetadataToTable(table: Table) {
    table.title = this.tableForm.controls.title.value;
    table.description = this.tableForm.controls.description.value;
    table.createdAt = this.tableForm.controls.createdAt.value;
  }

  bindForm(table: Table) {
    this.tableForm.controls.createdAt.setValue(table.createdAt, {emitEvent: false});
    // this.tableForm.controls.createdBy.setValue(table.createdBy, {emitEvent: false});
    this.tableForm.controls.isDiagnosis.setValue(table.isDiagnosis, {emitEvent: false});
    this.tableForm.controls.title.setValue(table.title, {emitEvent: false});
    this.tableForm.controls.description.setValue(table.description, {emitEvent: false});
    this.tableForm.controls.biblioSource.setValue('', {emitEvent: false});
    this.tableForm.controls.isDiagnosis.setValue(table.isDiagnosis, {emitEvent: false});
  }

}
