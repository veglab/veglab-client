import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRClusteringComponent } from './table-r-clustering.component';

describe('TableRClusteringComponent', () => {
  let component: TableRClusteringComponent;
  let fixture: ComponentFixture<TableRClusteringComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TableRClusteringComponent]
    });
    fixture = TestBed.createComponent(TableRClusteringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
