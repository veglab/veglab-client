import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  FormControl,
  ValidationErrors,
  Validators
} from '@angular/forms';
import { Subscription } from 'rxjs';
import * as _ from 'lodash-es';
import { environment } from 'src/environments/environment';

import { TableService } from '../../_services/table.service';
import { SyeService } from '../../_services/sye.service';
import { UserService } from '../../_services/user.service';
import { IdentificationService } from '../../_services/identification.service';
import { RClusteringResponse, RService } from '../../_services/r.service';
import { NotificationService } from '../../_services/notification.service';

import { Table } from '../../_models/table.model';
import { TableActionEnum } from '../../_enums/table-action-enum';
import { OccurrenceModel } from '../../_models/occurrence.model';

@Component({
  selector: 'vl-table-r-clustering',
  templateUrl: './table-r-clustering.component.html',
  styleUrls: ['./table-r-clustering.component.scss']
})
export class TableRClusteringComponent implements OnInit, OnDestroy {
  tableDataViewSubscriber: Subscription;
  currentTable: Table;
  isLoadingData = false;
  rData: RData = null;
  rResponse: RClusteringResponse = null;
  graphType: 'dendro'|'heatmap' = 'dendro';
  defaultHcutValue: number = null;
  applyClustersButtonDefaultLabel = 'Appliquer les clusters';
  applyClustersButtonLabel = this.applyClustersButtonDefaultLabel;
  groupedReleves: Array<{ group: number, releve: number; }>[] = [];
  dissimilarityMethods = ['aitchison', 'altGower', 'binomial', 'bray', 'canberra', 'cao', 'chao', 'chisq', 'chord', 'clark', 'euclidean', 'gower', 'hellinger', 'horn', 'jaccard', 'kulczynski', 'mahalanobis', 'manhattan', 'morisita', 'mountford', 'raup', 'robust.aitchison'];

  selectedStartColPosition: number = null;
  selectedEndColPosition: number = null;
  selectedSyeId: number = null;
  selectedOccurrenceIds = [];

  dissimilarityMethodControl = new FormControl<string>('jaccard', [Validators.required]);

  hCutFormControl = new FormControl<string>(this.defaultHcutValue ? this.defaultHcutValue.toString() : '', [Validators.required, this.hCutValidator]);

  public constructor(private tableService: TableService,
                     private syeService: SyeService,
                     private rService: RService,
                     private userService: UserService,
                     private identificationService: IdentificationService,
                     private notificationService: NotificationService) { }

  public ngOnInit() {
    this.tableService.tableSelectionElement.subscribe(data => {

      if (data.element === 'column') {
        this.selectedStartColPosition = data.startPosition;
        this.selectedEndColPosition = data.endPosition;
        this.selectedSyeId = data.syeId;
        this.selectedOccurrenceIds = data.occurrenceIds;
      } else {
        this.selectedStartColPosition = null;
        this.selectedEndColPosition = null;
        this.selectedSyeId = null;
        this.selectedOccurrenceIds = [];
      }

      if (null !== this.selectedStartColPosition && null !== this.selectedEndColPosition && null !== this.selectedSyeId) {
        if (data.occurrenceIds.length < 3) {
          this.updateDendrogramFromTable();
          return;
        }

        this.updateDendrogramFromSelection(data.occurrenceIds);
      }
    });

    this.tableDataViewSubscriber = this.tableService.tableDataView.subscribe(table => {
      this.updateDendrogramFromTable();
    });
  }

  private updateDendrogramFromTable() {
    this.selectedStartColPosition = null;
    this.selectedEndColPosition = null;
    this.selectedSyeId = null;
    this.selectedOccurrenceIds = [];

    this.rData = null;
    this.rResponse = null;
    this.groupedReleves = [];
    this.applyClustersButtonLabel = this.applyClustersButtonDefaultLabel;
    this.currentTable = this.tableService.getCurrentTable(); // @Todo only update when table change !
    this.rData = this.tableToRData(this.currentTable);

    if (this.rData) {
      this.getClusterData();
    }
  }

  private updateDendrogramFromSelection(data: Array<number>) {
    this.rData = null;
    this.rResponse = null;
    this.groupedReleves = [];
    this.applyClustersButtonLabel = this.applyClustersButtonDefaultLabel;
    this.currentTable = this.tableService.getCurrentTable(); // @Todo only update when table change !
    this.rData = this.occurrencesToRData(this.tableService.getTableOccurrencesByIds(this.currentTable, data));

    if (this.rData) {
      this.getClusterData();
    }
  }

  public ngOnDestroy() {
    if (this.tableDataViewSubscriber) { this.tableDataViewSubscriber.unsubscribe(); }
  }

  private getClusterData(): void {
    if (this.rData && this.rData.dataset && this.rData.dataset.releves && this.rData.dataset.releves.length < 3) {
      return;
    }
    this.isLoadingData = true;
    this.rService.getCluster(this.rData).subscribe(result => {
      this.isLoadingData = false;
      this.rResponse = result;
      if (this.rResponse.groups && this.rResponse.groups.length > 0) {
        const groups = _.groupBy(this.rResponse.groups, 'group');
        const groupsArray = Object.values(groups); // Convert Dictionary to array
        this.groupedReleves = groupsArray.map(ga => ga.map(gai => {
          return {
            group: gai.group,
            releve: +gai.releve
          };
        }));
        if (groupsArray) {
          const countGroups = groupsArray.length;
          this.applyClustersButtonLabel = `Appliquer les ${countGroups} clusters`;
        }
      }
    }, error => {
      // @TODO notify user
      this.isLoadingData = false;
      console.log(error);
    });
  }

  applyClusters(): void {
    if (!this.currentTable.ownedByCurrentUser) {
      this.notificationService.warn('Vous ne pouvez pas modifier un tableau qui dont vous n\'êtes pas l\'auteur.');
      return;
    }
    const tableRelevesCount = this.tableService.getReleves().length;
    if (null !== this.selectedStartColPosition && null !== this.selectedEndColPosition && null !== this.selectedSyeId) {
      // Apply clusters to selection
      this.applyClustersToSelection();
    } else {
      // Apply clusters to table
      this.applyClustersToTable();
    }
  }
  private applyClustersToTable() {
    const clonedTable = _.cloneDeep(this.currentTable);
    if (!this.groupedReleves || this.groupedReleves.length === 0) {
      this.notificationService.warn('Impossible d\'appliquer les clusters au tableau courant.');
      return;
    }

    const occurrences = this.tableService.getAllOccurrences(clonedTable);
    const syntheticSyes = clonedTable.sye.filter(sye => sye.syntheticSye); // @Todo check a real case
    clonedTable.sye = [];
    for (const group of this.groupedReleves) {
      const releveIdsInThisGroup = group.map(g => g.releve);
      const occurrencesToAddInSye = occurrences.filter(occ => releveIdsInThisGroup.includes(occ.id));

      const newSye = this.syeService.createSye();
      newSye.occurrences = occurrencesToAddInSye;
      clonedTable.sye.push(newSye);
      if (syntheticSyes && syntheticSyes.length > 0) {
        clonedTable.sye.push(...syntheticSyes);
      }
    }
    this.tableService.createSyntheticColumnsForSyeOnTable(clonedTable, this.userService.currentUser.getValue());
    this.tableService.createTableSyntheticColumn(clonedTable, this.userService.currentUser.getValue());
    this.tableService.updateColumnsPositions(clonedTable);
    this.tableService.updateSyeIds(clonedTable);
    this.tableService.setCurrentTable(clonedTable, true);

    // @Action
    this.tableService.createAction(TableActionEnum.groupColumnsByClustering);
  }

  private applyClustersToSelection() {
    const occurrences = this.tableService.getTableOccurrencesByIds(this.currentTable, this.selectedOccurrenceIds);

    // Remove selected occurrences from selected SyE
    this.syeService.removeOccurrencesByIds(this.tableService.getSyeById(this.currentTable, this.selectedSyeId), this.selectedOccurrenceIds);

    for (let i = this.groupedReleves.length - 1; i >= 0; i--) {
      const group = this.groupedReleves[i];
      const releveIdsInThisGroup = group.map(g => g.releve);
      const occurrencesToAddInSye = occurrences.filter(occ => releveIdsInThisGroup.includes(occ.id));

      const _newSye = this.syeService.createSye();
      _newSye.occurrences = occurrencesToAddInSye;
      this.currentTable.sye.splice(this.selectedSyeId, 0, _newSye);
    }

    // @Todo replace current table with a clone
    this.tableService.createSyntheticColumnsForSyeOnTable(this.currentTable, this.userService.currentUser.getValue());
    this.tableService.createTableSyntheticColumn(this.currentTable, this.userService.currentUser.getValue());
    this.tableService.updateColumnsPositions(this.currentTable);
    this.tableService.updateSyeIds(this.currentTable);
    this.tableService.setCurrentTable(this.currentTable, true);

    // @Action
    this.tableService.createAction(TableActionEnum.groupColumnsByClustering);

  }

  hCutValidator(control: FormControl): ValidationErrors | null {
    if (!control.value) { return null; }
    const isFloatingNumber = control.value.match('/^[+-]?\\d+(\\.\\d+)?$/');
    return isFloatingNumber ? null : { forbiddenValue: { value: control.value } };
  }

  hCutInputChange() {
    this.rData.hcut = +this.hCutFormControl.value;
  }

  clearHcut() {
    this.hCutFormControl.reset();
    this.rData.hcut = null;
    this.rResponse = null;
    this.getClusterData();
  }

  selectedDissimilarityMethodChanged() {
    this.rData.dissimilarityMethod = this.dissimilarityMethodControl.value;
    this.getClusterData();
  }

  switchGraph(): void {
    switch (this.graphType) {
      case 'dendro':
        this.graphType = 'heatmap';
        break;
      case 'heatmap':
        this.graphType = 'dendro';
        break;
    }
  }

  public getImagePath(): null|string {
    if (!this.rResponse) { return null; }
    let file = null;
    switch (this.graphType) {
      case 'dendro':
        file = this.rResponse.graphDendro;
        break;
      case 'heatmap':
        file = this.rResponse.graphHeatmap;
    }
    return `${environment.apiBaseUrl}/r/data/output/${file}`;
  }

  refreshGraphWithHcut(): void {
    this.getClusterData();
  }

  /**
   * Transform a Table to a RData model
   *
   * input table (Table):
   *         | 1234 | 5678 | <- occurrence (relevé) ids
   * taxon A |  +   |   3  |
   * taxon B |  1   |   4  |
   * taxon C |  2   |   5  |
   *
   * output (RData):
   * {
   *   dataset: {
   *     releves: [1234, 5678],
   *     taxons: ['taxon A', 'taxon B', 'taxon C'],
   *     values: [
   *       ['+', '1', '2'],
   *       ['3', '4', '5']
   *     ]
   *   }
   * }
   */
  tableToRData(table: Table): RData {
    const rData: RData = {
      dataset: {
        releves: [],
        taxons: [],
        values: []
      },
      dissimilarityMethod: this.dissimilarityMethodControl.value,
      hcut: this.hCutFormControl.value ? +this.hCutFormControl.value : null
    };

    for (const rd of table.rowsDefinition) {
      if (rd.type === 'data') {
        rData.dataset.taxons.push(rd.displayName);
      }
    }

    for (const sye of table.sye) {
      for (const rel of sye.occurrences) {
        rData.dataset.releves.push(rel.id);
        const occurrences = this.tableService.getChildOccurrences(rel);
        const occValues = [];
        for (const rd of table.rowsDefinition) {
          const occurrence = _.find(occurrences, occ => {
            const occIdentification = this.identificationService.getFavoriteIdentification(occ);
            if (occ.layer === rd.layer && occIdentification.repository === rd.repository && occIdentification.repositoryIdTaxo === rd.repositoryIdTaxo) {
              return true;
            }
          });
          if (rd.type === 'data' && occurrence && occurrence.coef) {
            occValues.push(1);
          } else if (rd.type === 'data' && !occurrence) {
            occValues.push(0);
          }
        }
        rData.dataset.values.push(occValues);
      }
    }

    return rData;
  }

  occurrencesToRData(occurrences: Array<OccurrenceModel>): RData {
    const rData: RData = {
      dataset: {
        releves: [],
        taxons: [],
        values: []
      },
      dissimilarityMethod: this.dissimilarityMethodControl.value,
      hcut: this.hCutFormControl.value ? +this.hCutFormControl.value : null
    };

    for (const rd of this.tableService.getCurrentTable().rowsDefinition) {
      if (rd.type === 'data') {
        rData.dataset.taxons.push(rd.displayName);
      }
    }

    for (const rel of occurrences) {
      rData.dataset.releves.push(rel.id);
      const _occurrences = this.tableService.getChildOccurrences(rel);
      const occValues = [];
      for (const rd of this.tableService.getCurrentTable().rowsDefinition) {
        const occurrence = _.find(_occurrences, occ => {
          const occIdentification = this.identificationService.getFavoriteIdentification(occ);
          if (occ.layer === rd.layer && occIdentification.repository === rd.repository && occIdentification.repositoryIdTaxo === rd.repositoryIdTaxo) {
            return true;
          }
        });
        if (rd.type === 'data' && occurrence && occurrence.coef) {
          occValues.push(1);
        } else if (rd.type === 'data' && !occurrence) {
          occValues.push(0);
        }
      }
      rData.dataset.values.push(occValues);
    }

    return rData;
  }
}

export interface RData {
  dataset: {
    releves: Array<any>,
    values: Array<Array<any>>,
    taxons: Array<string>
  };
  dissimilarityMethod: string;
  hcut: number|null;
}
