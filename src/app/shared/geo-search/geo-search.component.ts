import { Component, ElementRef, EventEmitter, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';

import { Observable, of, switchMap } from 'rxjs';
import { debounceTime, tap } from 'rxjs/operators';

import { GeoService } from '../../_services/geo.service';

import { CommonGeocodedData } from '../../_models/geo/commonGeocodedData.model';

@Component({
  selector: 'vl-geo-search',
  templateUrl: './geo-search.component.html',
  styleUrls: ['./geo-search.component.scss']
})
export class GeoSearchComponent {
  @ViewChild('searchInput') searchInput: ElementRef<HTMLInputElement>;
  @Output() geoData = new EventEmitter<Array<CommonGeocodedData>>();

  selectedLevel: 'all'|'country'|'region'|'county'|'city' = 'all';
  geoSearchCtrl = new FormControl('');
  filteredOptions: Observable<CommonGeocodedData[]>;
  isLoading = false;

  public constructor(public geoService: GeoService) {
    this.filteredOptions = this.geoSearchCtrl.valueChanges.pipe(
      tap(() => this.isLoading = true),
      debounceTime(300),
      switchMap(value => {
        if (!value || value.trim() === '') {
          this.isLoading = false;
          return of([]);
        } else {
          return this.geoService.geocodeSpecificUsingVlGeocoder(value, null, null, null, this.selectedLevel === 'all' ? null : this.selectedLevel);
        }
      }),
      tap(() => this.isLoading = false)
    );
  }

  public onOptionSelected(event: MatAutocompleteSelectedEvent) {
    this.geoData.next([(event.option.value) as CommonGeocodedData]);
    this.searchInput.nativeElement.value = '';
    this.geoSearchCtrl.setValue(null);
  }
}
