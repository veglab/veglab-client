import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TablesTableViewComponent } from './tables-table-view.component';

describe('TablesTableViewComponent', () => {
  let component: TablesTableViewComponent;
  let fixture: ComponentFixture<TablesTableViewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TablesTableViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablesTableViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
