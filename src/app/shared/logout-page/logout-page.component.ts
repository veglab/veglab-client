import { Component } from '@angular/core';
import { SsoService } from '../../_services/sso.service';
import { Router } from '@angular/router';

@Component({
  selector: 'vl-logout-page',
  templateUrl: './logout-page.component.html',
  styleUrls: ['./logout-page.component.scss']
})
export class LogoutPageComponent {
  constructor(
    private ssoService: SsoService,
    private router: Router) {
    this.logout();
  }
  logout() {
    setTimeout(() => {
      this.ssoService.logout();
    }, 1500);
  }
}
