interface VlGeocodedSharedDataModel {
  id: number;
  name: string;
  level: 'country'|'region'|'county'|'city',
  countrycode: string;
  country: string;
  regioncode: string;
  region: string;
  countycode: string;
  county: string;
  citycode: string;
  city: string;
  postcode: string;
  geojson: any;
  centroid: any;
}
export interface VlGeocodedDataModel extends VlGeocodedSharedDataModel {
  levenshtein_distance: number;
}

// tslint:disable-next-line:no-empty-interface
export interface VlReverseGeocodedDataModel extends VlGeocodedSharedDataModel { }
