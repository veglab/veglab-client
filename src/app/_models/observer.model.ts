export interface Observer {
  id: number;
  uuid?: string;
  name: string;
}
