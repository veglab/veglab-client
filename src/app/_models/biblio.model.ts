export interface Biblio {
  '@context'?: string;
  '@id'?:      string;
  '@type'?:    string;

  id: number;
  uuid?: string;
  title: string;
}
