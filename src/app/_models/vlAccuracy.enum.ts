export enum VlAccuracyEnum {
  EXACT       = 'EXACT',        // "Précise": Geometry was provided by the user
  PLACE       = 'PLACE',        // "Lieu-dit"
  CITY        = 'CITY',         // "ville"|"village"|...
  COUNTY      = 'COUNTY',       // "département"|"land"|...
  REGION      = 'REGION',       //
  COUNTRY     = 'COUNTRY',
  OTHER       = 'OTHER/UNKNOWN'
}
