export interface BoundaryModel {
  id: number;
  name?: string;
  level?: 'country' | 'region' | 'county' | 'city';
  countryCode?: string;
  regionCode?: string;
  countyCode?: string;
  cityCode?: string;
  postcode?: string;
  geojson?: any;
  centroid: any;
}
