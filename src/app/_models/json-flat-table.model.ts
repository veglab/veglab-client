export interface JsonFlatTable {
  rows: Array<JsonFlatTableRow>;
}

export interface JsonFlatTableRow {
  type?: 'meta'|'group'|'data';
  repository?: string;
  idNomen?: string;
  idTaxo?: string;
  layer?: string;
  label: string;
  columns? : Array<{
    type?: 'occurrence'|'syntheticColumn',
    label?: string
  }>;
}
