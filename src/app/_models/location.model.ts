import { VlAccuracyEnum } from './vlAccuracy.enum';
import { CommonGeocodedData } from './geo/commonGeocodedData.model';

export interface LocationModel {
  geometry: any;
  centroid: any;

  isLatLngInitialSetByUser: boolean;
  citedLocation: string;
  locationAccuracy: VlAccuracyEnum;

  country: string;
  countryCode: string;
  region: string;
  regionCode: string;
  county: string;
  countyCode: string;
  city: string;
  cityCode: string;
  postcode: string;

  boundaryId: number;

  suggestedLocations: Array<{ location: CommonGeocodedData, readableAddress: string }>;
  selectedLocation: { location: CommonGeocodedData, readableAddress: string };
}
