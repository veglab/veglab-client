/**
 * Type safe location data
 */
export interface CommonGeocodedData {
  id: number;

  level: 'country'|'region'|'county'|'city';

  city: string;
  county: string;
  region: string;
  country: string;

  postCode: string;

  cityCode: string;
  countyCode: string;
  regionCode?: string;
  countryCode: string;

  geometry: any;
  centroid: any;
}
