import { RepositoryItemModel } from '../_models/repository-item.model';

export interface TableRelatedSyntaxon {
  isDiagnosis: boolean;
  identification: RepositoryItemModel;
  pdf?: {
    file: File,
    formData: FormData,
    uploadStatus: 'staging' | 'uploading' | 'uploaded'
  };
}
