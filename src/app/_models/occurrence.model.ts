import { IdentificationModel } from './identification.model';
import { ExtendedFieldOccurrence } from './extended-field-occurrence';

import { Certainly } from '../_enums/certainly-enum';
import { OccurrenceType } from '../_enums/occurrence-type-enum';
import { Phenology } from '../_enums/phenology-enum';
import { InputSource } from '../_enums/input-source-enum';
import { PublishedLocation } from '../_enums/published-location-enum';
import { LocationAccuracy } from '../_enums/location-accuracy-enum';
import { Level } from '../_enums/level-enum';
import { LayerEnum } from '../_enums/layer-list';
import { VlAccuracyEnum } from '../_models/vlAccuracy.enum';
import { Observer } from './observer.model';
import { Biblio } from './biblio.model';
import { VlUser } from './vl-user.model';
import { BoundaryModel } from './boundary.model';


export interface OccurrenceModel {
  id?: number;
  uuid?: string;
  userId: string;

  originalReference?: string;  // needed for table import

  userEmail: string;
  userPseudo?: string;
  owner: VlUser;

  observer: string;
  observerInstitution?: string;
  vlObservers?: Array<Observer>;

  dateObserved?: Date;
  dateObservedPrecision?: 'day' | 'month' | 'year';
  dateCreated: Date;
  dateUpdated?: Date;
  datePublished?: Date;

  userSciName?: string;     // @todo force nullable=false for VegLab ?
  userSciNameId?: number;   // @todo force nullable=false for VegLab ?
  acceptedSciName?: string;
  acceptedSciNameId?: number;
  plantnetId?: number;

  family?: string;

  taxoRepo: string;

  certainty?: Certainly;

  annotation?: string;

  occurenceType?: OccurrenceType;
  isWild?: boolean;

  coef?: string;      // @todo change to string

  phenology?: Phenology;
  sampleHerbarium?: boolean;

  bibliographySource?: string;
  vlBiblioSource?: Biblio;
  inputSource: InputSource;

  isPublic: boolean;
  isVisibleInCel: boolean;
  isVisibleInVegLab: boolean;
  signature: string;  // @todo fullfiled in the backend ?

  geometry?: any;  // geoJson object
  centroid?: any;  // geoJson object
  elevation?: number;
  isElevationEstimated?: boolean;
  citedLocation?: string;
  locationAccuracy?: VlAccuracyEnum;
  boundary?: any;
  countryCode?: string;
  country?: string;
  regionCode?: string;
  region?: string;
  countyCode?: string;
  county?: string;
  cityCode?: string;
  city?: string;
  postcode?: string;

  identiplanteScore?: number;
  isIdentiplanteValidated: boolean;
  identificationAuthor?: string;

  project?: string;      // IRI
  userProfile: Array<number>;  // ???
  photos?: Array<any>;
  identifications?: Array<IdentificationModel>;

  delUpdateNotifications: any; // I don't care

  extendedFieldOccurrences?: Array<ExtendedFieldOccurrence>;

  parent?: OccurrenceModel;
  parentId?: number;        // Provided by ES
  level: Level;
  parentLevel: Level;
  layer?: LayerEnum;
  children?: Array<OccurrenceModel>;
  childrenIds?: Array<number>;

  childrenPreview?: any;    // ES only

  vlWorkspace: string;
}
