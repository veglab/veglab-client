import { VlUser } from './vl-user.model';

export interface IdentificationModel {
  id?:               number;
  uuid?:             string;
  createdAt:       Date;
  owner:             VlUser;
  updatedBy?:        string;    // user id
  updatedAt?:        Date;
  repository:        string;
  repositoryIdNomen: number;
  repositoryIdTaxo?: string;
  citationName:      string;
  nomenclaturalName: string;
  taxonomicalName:   string;
  // isDiagnosis?:      boolean;
}
