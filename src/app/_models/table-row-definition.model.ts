export interface TableRowDefinition {
  id:                number;
  rowId:             number;
  type:              'group' | 'data';
  groupId:           number;
  groupTitle:        string;
  layer:             string;
  displayName:       string;
  repository:        string;
  repositoryIdNomen: number;
  repositoryIdTaxo:  string;
  children?:         Array<TableRowDefinition>;
  aggregated?:       boolean;
  expanded?:         boolean;
  createdForAggregation?: boolean;
}

/**
 * Handsontable's data view is an array of TableRow
 */
export interface TableRow extends TableRowDefinition {
  count:          number;
  items: Array<{
    type:         'cellOccValue' | 'cellSynColValue' | 'rowTitle' | 'rowValue' | 'rowValueAggregate' | 'rowValueAggregated';
    syeId:        number;
    syntheticSye: boolean;
    occurrenceId: number;
    value:        string
  }>;
}
