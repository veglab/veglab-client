import { RepositoryItemModel } from './repository-item.model';
import { Observable } from 'rxjs';

/**
 * Each repository service must implement this interface
 */
export interface RepositoryModel {

  /**
   * Identifier of the repository
   */
  id: string;

  /**
   * Name of the repository
   */
  label: string;

  /**
   * The URL of the APi
   */
  apiUrl: string;

  /**
   * The URL of the API that is used to retrieve a valid occurrence
   * Optional : apiUrl could already send valid occurrence for a given synonym occurrence
   */
  apiUrlValidoccurrence?: string;

  /**
   * Which levels can this repository manage ?
   * Array must contain 'idiotaxon' | 'synusy' | 'microcenosis' | 'phytocenosis' | ...
   */
  levels: Array<string>;

  /**
   * French description
   */
  description_fr: string;
  /**
   * Call the API and return an Observable
   * Perform as you want, no matter how the data are organized
   */
  findElement: (query: string) => Observable<any>;

  /**
   * Call the API and return an Observable
   */
  findById: (id: number | string) => Observable<any>;

  /**
   * Call the API and return an Observable
   */
  findByIdNomen: (id: number | string) => Observable<any>;

  /**
   * Call the API and return an Observable
   */
  findByIdTaxo: (id: number | string) => Observable<any>;

  /**
   * Find a valid occurrence by its nomenenclatural id
   * This method is different from findByIdNomen because its instantiation could use another API
   * It's optional because the initial methods could already return valid occurrence for a given synonym
   * If you implement this method, it overrides findValidOccurrenceByIdTaxo()
   */
  findValidOccurrenceByIdNomen?: (id: number | string) => Observable<any>;

  /**
   * Find a valid occurrence by its taxonomical id
   * This method is different from findByIdTaxo because its instantiation could use another API
   * It's optional because the initial methods could already return valid occurrence for a given synonym
   * Pay attention ! If you implement both this method and findValidOccurrenceByIdNomen(), findValidOccurrenceByIdNomen() has priority
   * so this method will never be used
   */
  findValidOccurrenceByIdTaxo?: (id: number | string) => Observable<any>;

  /**
   * In case of the data are not ready to use (e.g. nested into another object)
   * Return results
   * Basically, could return input data (return rawData)
   */
  filter: (rawData) => any;

  /**
   * The application processes each data form a repository of the same way
   * So these data must share a common model : RepositoryItemModel
   */
  standardize: (rawData: any) => Array<RepositoryItemModel>;

  /**
   * The shared RepositoryItemModel also applies for valid occurrences
   * Optional : see comment of findValidOccurrenceByIdNomen()
   */
  standardizeValidOccurrence?: (rawData: any) => RepositoryItemModel;

}
