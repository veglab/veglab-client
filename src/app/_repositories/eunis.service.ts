import { Injectable } from '@angular/core';
import { RepositoryModel } from '../_models/repository.model';
import { RepositoryItemModel } from '../_models/repository-item.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

/**
 * EUNIS
 */
@Injectable({
  providedIn: 'root'
})
export class EunisRepositoryService implements RepositoryModel {
  id = 'eunis';
  label = 'EUNIS';
  apiUrl = `${environment.repoApi.basePath}/eunis`;
  apiUrl2 = `${environment.repoApi.basePath}/eunis`;
  levels = ['microcenosis', 'synusy'];
  description_fr = `Classification des habitats EUNIS 2022 pour la France.`;

  constructor(private http: HttpClient) {
  }

  findElement = (term: string) => {
    const headers = new HttpHeaders({'Content-type': 'application/json'});
    const request: Observable<any> = this.http.get(`${this.apiUrl}/term/${term}`, {headers});
    return request;
  }

  findById(id) {
    return this.findByIdNomen(id);
  }

  findByIdNomen(idNomen) {
    const headers = new HttpHeaders({'Content-type': 'application/json'});
    const request: Observable<any> = this.http.get(`${this.apiUrl}/nomen/${idNomen}`, {headers});
    return request;
  }

  findByIdTaxo(idTaxo) {
    const headers = new HttpHeaders({'Content-type': 'application/json'});
    const request: Observable<any> = this.http.get(`${this.apiUrl}/taxo/${idTaxo}`, {headers});
    return request;
  }

  standardize = (rawData: Array<Eunis>, attachRawData: boolean = false): any => {
    const sData: Array<RepositoryItemModel> = [];
    // Get results from elasticsearch (= remove metadata)
    rawData = this.filter(rawData);

    rawData.forEach((item) => {
      const rim: RepositoryItemModel = {
        repository: null,
        name: null,
        author: null,
        idTaxo: null,
        idNomen: null,
        isSynonym: false,
        rawData: null
      };
      rim.name = `[${item.lb_code}] ${item.lb_hab_fr}`;
      rim.author = item.lb_auteur;
      rim.idTaxo = item.cd_hab;
      rim.idNomen = item.cd_hab;
      if (attachRawData) {
        rim.rawData = item;
      }
      sData.push(rim);
    });

    return sData;
  }

  /**
   * Before returning an Observable, some data may need to be transformed
   */
  filter(data) {
    return data;
  }

}

interface Eunis {
  cd_hab: number;
  fg_validite: string;
  cd_typo: number;
  lb_code: string;
  lb_hab_fr: string;
  lb_hab_fr_complet: string;
  lb_hab_en: string;
  lb_auteur: string;
  niveau: number;
  lb_niveau: string;
  cd_hab_sup: number;
  path_cd_hab: string;
  france: boolean;
  lb_description: string;
}
