import { Injectable } from '@angular/core';
import { RepositoryModel } from '../_models/repository.model';
import { RepositoryItemModel } from '../_models/repository-item.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

/**
 * Baseveg
 */
@Injectable({
  providedIn: 'root'
})
export class CatvegRepositoryService implements RepositoryModel {
  id = 'catveg';
  label = 'CatVeg';
  apiUrl = `${environment.repoApi.basePath}/catveg`;
  apiUrl2 = `${environment.repoApi.basePath}/catveg`;
  levels = ['synusy', 'microcenosis'];
  description_fr = 'Catalogue de la végétation de France métropolitaine';

  constructor(private http: HttpClient) {
  }

  findElement = (term: string) => {
    const headers = new HttpHeaders({'Content-type': 'application/json'});
    const request: Observable<any> = this.http.get(`${this.apiUrl}/term/${term}`, {headers});
    return request;
  }

  findById(id) {
    return this.findByIdNomen(id);
  }

  findByIdNomen(idNomen) {
    const headers = new HttpHeaders({'Content-type': 'application/json'});
    const request: Observable<any> = this.http.get(`${this.apiUrl}/nomen/${idNomen}`, {headers});
    return request;
  }

  findByIdTaxo(idTaxo) {
    const headers = new HttpHeaders({'Content-type': 'application/json'});
    const request: Observable<any> = this.http.get(`${this.apiUrl}/taxo/${idTaxo}`, {headers});
    return request;
  }

  standardize = (rawData: Array<CatVeg>, attachRawData: boolean = false): Array<RepositoryItemModel> => {
    const sData: Array<RepositoryItemModel> = [];
    // Get results from elasticsearch (= remove metadata)
    rawData = this.filter(rawData);

    rawData.forEach((item) => {
      const rim: RepositoryItemModel = {
        repository: null,
        name: null,
        author: null,
        idTaxo: null,
        idNomen: null,
        isSynonym: false,
        rawData: null
      };
      rim.name = item.lb_nom;
      rim.author = item.autorites;
      rim.idTaxo = item.cd_nom;
      rim.idNomen = item.cd_nom;
      rim.isSynonym = false;
      if (attachRawData) {
        rim.rawData = item;
      }
      sData.push(rim);
    });

    return sData;
  }

  standardizeValidOccurrence = (rawData: CatVeg): RepositoryItemModel => {
    const results = this.standardize([rawData]);
    if (results.length > 1) {
      // @todo throw error
      return results[0];
    } else {
      return results[0];
    }
  }

  /**
   * Before returning an Observable, some data may need to be transformed
   */
  filter(data) {
    return data;
  }

}

interface CatVeg {
  cd_nom: number;
  cd_sup: number;
  rang: number;
  lb_nom: string;
  autorites: string;
  pp: string;
  remarques_nomenc: string;
}
