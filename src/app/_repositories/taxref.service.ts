import { Injectable } from '@angular/core';
import { RepositoryModel } from '../_models/repository.model';
import { RepositoryItemModel } from '../_models/repository-item.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

/**
 * Taxref
 */
@Injectable({
  providedIn: 'root'
})
export class TaxrefRepositoryService implements RepositoryModel {
  id = 'taxref';
  label = 'Taxref';
  apiUrl = `${environment.repoApi.basePath}/taxref`;
  apiUrl2 = `${environment.repoApi.basePath}/taxref`;
  levels = ['idiotaxon'];
  description_fr = `Référentiel taxonomique pour la France : méthodologie, mise en œuvre et diffusion. Muséum national d’Histoire naturelle, Paris.`;

  constructor(private http: HttpClient) {
  }

  findElement = (term: string) => {
    const headers = new HttpHeaders({'Content-type': 'application/json'});
    const request: Observable<any> = this.http.get(`${this.apiUrl}/term/${term}`, {headers});
    return request;
  }

  findById(id) {
    return this.findByIdNomen(id);
  }

  findByIdNomen(idNomen) {
    const headers = new HttpHeaders({'Content-type': 'application/json'});
    const request: Observable<any> = this.http.get(`${this.apiUrl}/nomen/${idNomen}`, {headers});
    return request;
  }

  findByIdTaxo(idTaxo) {
    const headers = new HttpHeaders({'Content-type': 'application/json'});
    const request: Observable<any> = this.http.get(`${this.apiUrl}/taxo/${idTaxo}`, {headers});
    return request;
  }

  standardize = (rawData: Array<Taxref>, attachRawData: boolean = false): any => {
    const sData: Array<RepositoryItemModel> = [];
    // Get results from elasticsearch (= remove metadata)
    rawData = this.filter(rawData);

    rawData.forEach((item) => {
      const rim: RepositoryItemModel = {
        repository: null,
        name: null,
        author: null,
        idTaxo: null,
        idNomen: null,
        isSynonym: false,
        rawData: null
      };
      rim.name = item.lb_nom;
      rim.author = item.lb_auteur;
      rim.idTaxo = item.cd_ref;
      rim.idNomen = Number(item.cd_nom);
      if (attachRawData) {
        rim.rawData = item;
      }
      sData.push(rim);
    });

    return sData;
  }

  /**
   * Before returning an Observable, some data may need to be transformed
   */
  filter(data) {
    return data;
  }

}

interface Taxref {
  cd_nom: number;
  cd_taxsup: number;
  cd_sup: number;
  cd_ref: number;
  regne: string;
  phyllum: string;
  classe: string;
  ordre: string;
  famille: string;
  sous_famille: string;
  tribu: string;
  group1_inpn: string;
  group2_inpn: string;
  group3_inpn: string;
  rang: string;
  lb_nom: string;
  lb_auteur: string;
  nom_complet: string;
  nom_complet_html: string;
  nom_valide: string;
  nom_vern: string;
  nom_vern_eng: string;
  habitat: number;
  fr: string;
  gf: string;
  mar: string;
  gua: string;
  sm: string;
  sb: string;
  spm: string;
  may: string;
  epa: string;
  reu: string;
  sa: string;
  ta: string;
  taaf: string;
  pf: string;
  nc: string;
  wf: string;
  cli: string;
  url: string;
}
