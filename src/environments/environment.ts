// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseUrl: 'https://${CLIENT_HOST}',
  apiBaseUrl: 'https://${API_HOST}',
  esAuthorizationPassword: '${ELASTIC_PASSWORD}',
  esBaseUrl: 'https://${ES_HOST}',
  esBaseflorTraitsApi: 'https://${ES_REPO_HOST}/baseflor',
  // pdfBaseUrl: 'https://${API_HOST}/media/veglab/pdf/',
  esIndexes: {
    users: { name: '${FOS_ELASTICA_INDEXES_USERS_INDEX_NAME}' },
    occurrences: { name: '${FOS_ELASTICA_INDEXES_OCCURRENCES_INDEX_NAME}' },
    syntheticColumns: { name: '${FOS_ELASTICA_INDEXES_SYNTHETIC_COLUMNS_INDEX_NAME}' },
    tables: { name: '${FOS_ELASTICA_INDEXES_TABLES_INDEX_NAME}' },
    observers: { name: '${FOS_ELASTICA_INDEXES_OBSERVERS_INDEX_NAME}' },
    biblioPhyto: { name: '${FOS_ELASTICA_INDEXES_BIBLIO_PHYTO_INDEX_NAME}' }
  },
  app : {
    title:           'vegLab',
    unsetTokenValue: 'unset',
    absoluteBaseUrl: '',
  },
  sso: {
    clientId:         '${SSO_CLIENT_ID}',
    realmId:          '${KEYCLOAK_REALM}',
    baseUrl:          'https://${SSO_HOST}${SSO_URI}',
    loginEndpoint:    'https://${SSO_HOST}${SSO_URI}${SSO_LOGIN_ENDPOINT}',
    logoutEndpoint:   'https://${SSO_HOST}${SSO_URI}${SSO_LOGOUT_ENDPOINT}',
    identiteEndpoint: 'https://${SSO_HOST}${SSO_URI}${SSO_REFRESH_ENDPOINT}',
    refreshEndpoint:  'https://${SSO_HOST}${SSO_URI}${SSO_REFRESH_ENDPOINT}',
    refreshInterval:  '${SSO_REFRESH_INTERVAL}',
    authentificationPage: 'https://${SSO_HOST}/realms/${KEYCLOAK_REALM}/login-actions/authenticate?client_id=account',
    registrationPage: 'https://${SSO_HOST}/realms/${KEYCLOAK_REALM}/login-actions/registration?client_id=account',
    roles: {
      admin: '${SSO_ROLE_ADMIN}'
    }
  },
  mapQuestApiKey: '${MAPQUEST_API_KEY}',
  repo: {
    sporeElasticHost: 'https://${ES_REPO_HOST}',
    defaultIdiotaxonRepository: 'taxref',
    defaultSyntaxonRepository: 'catveg'
  },
  repoApi: {
    basePath: 'https://${GEO_API_HOST}'
  },
  geoApi: {
    basePath: 'https://${GEO_API_HOST}'
  },
  rApi: {
    basePath: 'https://${R_API_HOST}'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
